package org.gcube.oidc.rest;

import java.net.MalformedURLException;
import java.net.URL;

public class RestHelperTest {

    public RestHelperTest() {
    }

    //    @Test
    public void getAvatar() throws MalformedURLException {
        String accessTokenBearer = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE1OTc0MTc4MzEsImlhdCI6MTU5NzQxNzUzMSwiYXV0aF90aW1lIjoxNTk3NDE3NTI5LCJqdGkiOiI5ZjAwMzM1Yy1jM2NlLTQ3ZTktYTRhZS05MmM4ZDc1NmUyOWMiLCJpc3MiOiJodHRwczovL2FjY291bnRzLmRldi5kNHNjaWVuY2Uub3JnL2F1dGgvcmVhbG1zL2Q0c2NpZW5jZSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiIzZTcwMzBhNS00M2MzLTRiNjUtYWMwYS0xNTJkYmU3MDI0NTIiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJwcmVwcm9kMi5kNHNjaWVuY2Uub3JnIiwic2Vzc2lvbl9zdGF0ZSI6IjAxOWEzMTVjLTY3MTQtNDQyZi1hNDE2LWM4MjAxNWFhYzFmNSIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIGVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6Ik1hdXJvIE11Z25haW5pIiwicHJlZmVycmVkX3VzZXJuYW1lIjoidGhlbWF4eDc2IiwiZ2l2ZW5fbmFtZSI6Ik1hdXJvIiwiZmFtaWx5X25hbWUiOiJNdWduYWluaSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHaUFxUzZRc00xaDNXV1BqMU15VUdlRFA2VnFaQlhtOTZ4cHhZLXNndyIsImVtYWlsIjoidGhlbWF4eDc2QGdtYWlsLmNvbSJ9.WMddlUQujlpmzW07Lrk50vOyWpiT1Tp_RsBWRbzyrQnu5EQQSCq1uGOuSf7Z3VZFv8fbnzWekMZRNzhngEddzOQHgAlsgRdqNI_-ucjmb_8SfR2I5PkYJLTG0jF-Urqi-GvfJtLr2B8dBDnMDO6FLFsg1e5qb-5HkV60eEtY2Wult1PGxlkD05w-K2w513IOMkVIl25ZxKbP61-Iu1qfV_q3QFvUHl_pdqL7uKC5bkl1lqTVeuCwrXrKubHnKc-UzpHtHp8XY0Iao7LdLtON7SODYhU8EkZ860ZlFTSCszmLUpSH4t_shSk9Fiqd8wBKAet5ngmyAPzKx9TT2FK65g";
        URL avatarURL = new URL("https://accounts.dev.d4science.org/auth/realms/d4science/avatar-provider/");

        byte[] avatarBytes = OpenIdConnectRESTHelper.getUserAvatar(avatarURL, accessTokenBearer);
        //        assertNotNull(avatarBytes);
    }

    //  @Test
    public void getUMATokenWithBasicAuth()
            throws MalformedURLException, OpenIdConnectRESTHelperException, InterruptedException {
        URL tokenURL = new URL(
                "https://accounts.dev.d4science.org/auth/realms/d4science/protocol/openid-connect/token");
        JWTToken token = OpenIdConnectRESTHelper.queryUMAToken(tokenURL, "storage-manager-trigger",
                "e2591a99-b694-4dbe-8f7b-9755a3db80af",
                "/gcube", null);
        System.out.println(token.getAccessTokenString());
    }

    //    @Test
    public void getExp() throws MalformedURLException, OpenIdConnectRESTHelperException, InterruptedException {
        URL tokenURL = new URL(
                "https://accounts.dev.d4science.org/auth/realms/d4science/protocol/openid-connect/token");
        JWTToken token = OpenIdConnectRESTHelper.queryClientToken("robcomp", "0fec31cb-23c3-44e2-9359-d6db6784b7d3",
                tokenURL);
        System.out.println(token.getExpAsDate());
        System.out.println(token.getAzp());
        //        Thread.sleep((token.getExp() * 1000 - System.currentTimeMillis() + 5000));
        System.out.println(token.isExpired());
    }

    public static void main(String[] args) throws Exception {
        RestHelperTest rht = new RestHelperTest();
        //        rht.getAvatar();
        try {
            rht.getUMATokenWithBasicAuth();
        } catch (OpenIdConnectRESTHelperException e) {
            if (e.hasJSONPayload()) {
                System.out.println("JSON response: " + e.getResponseString());
            } else {
                System.out.println("Plain text response: " + e.getResponseString());
            }
        }
    }
}
