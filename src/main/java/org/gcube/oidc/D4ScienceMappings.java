package org.gcube.oidc;

import java.util.HashSet;
import java.util.Set;

public class D4ScienceMappings {

    public enum Role {

        MEMBER("Member"),

        ACCOUNTING_MANAGER("Accounting-Manager"),
        CATALOGUE_ADMIN("Catalogue-Admin"),
        CATALOGUE_EDITOR("Catalogue-Editor"),
        CATALOGUE_MANAGER("Catalogue-Manager"),
        CATALOGUE_MODERATOR("Catalogue-Moderator"),
        DATA_EDITOR("Data-Editor"),
        DATA_MANAGER("Data-Manager"),
        DATAMINER_MANAGER("DataMiner-Manager"),
        INFRASTRUCTURE_MANAGER("Infrastructure-Manager"),
        VO_ADMIN("VO-Admin"),
        VRE_DESIGNER("VRE-Designer"),
        VRE_MANAGER("VRE-Manager");

        private static Set<String> STRING_VALUES;

        private String str;

        Role(String str) {
            this.str = str;
        }

        public String asString() {
            return str;
        }

        static {
            STRING_VALUES = new HashSet<String>();
            for (Role role : Role.values()) {
                STRING_VALUES.add(role.asString());
            }
        }

        public static Set<String> stringValues() {
            return STRING_VALUES;
        }

        public static boolean exists(String role) {
            return STRING_VALUES.contains(role);
        }

    }

}
