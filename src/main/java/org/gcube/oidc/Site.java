package org.gcube.oidc;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Site implements Comparable<Site> {

    private String name;
    private List<String> roles;
    private Map<String, Site> children;

    public Site(String name, List<String> roles) {
        this.name = name;
        this.roles = roles;
        this.children = new TreeMap<>();
    }

    public String getName() {
        return name;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getRoles() {
        return roles;
    }

    public Map<String, Site> getChildren() {
        return children;
    }

    public String dump() {
        return dump("");
    }

    protected String dump(String indentString) {
        StringBuilder sb = new StringBuilder();
        sb.append(indentString + "name: " + getName() + ", roles: " + getRoles() + "\n");
        for (String child : getChildren().keySet()) {
            sb.append(getChildren().get(child).dump(indentString + "  "));
        }
        return sb.toString();
    }


    @Override
    public int compareTo(Site o) {
        return getName().compareTo(o.getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Site) {
            Site siteObj = (Site) obj;
            return getName().equals(siteObj.getName()) && getRoles().equals(siteObj.getRoles())
                    && getChildren().equals(siteObj.getChildren());
        } else {
            return false;
        }
    }

}