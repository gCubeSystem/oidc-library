package org.gcube.oidc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class URLEncodedContextMapper extends SlashSeparatedContextMapper {

    public URLEncodedContextMapper(Map<String, List<String>> resourceName2AccessRoles) {
        super(decodeContextNames(resourceName2AccessRoles));
    }

    private static Map<String, List<String>> decodeContextNames(Map<String, List<String>> resourceName2AccessRoles) {
        Map<String, List<String>> decodedResourceName2AccessRoles = new TreeMap<>();
        for (String encodedContext : resourceName2AccessRoles.keySet()) {
            try {
                decodedResourceName2AccessRoles.put(URLDecoder.decode(encodedContext, "UTF-8"),
                        resourceName2AccessRoles.get(encodedContext));
            } catch (UnsupportedEncodingException e) {
                logger.error("Cannot URL decode context name: " + encodedContext, e);
            }
        }
        return decodedResourceName2AccessRoles;
    }

}
