package org.gcube.oidc.rest;

public class OpenIdConnectRESTHelperException extends Exception {

    private static final long serialVersionUID = -1615745541003534684L;

    private int status = -1;
    private String contentType = null;
    private String responseString = null;

    public static OpenIdConnectRESTHelperException create(String message, int status, String contentType,
            String textResponse) {

        OpenIdConnectRESTHelperException e = new OpenIdConnectRESTHelperException(
                "[" + status + "] " + message + " (" + contentType + ") :" + textResponse);

        e.setStatus(status);
        e.setContentType(contentType);
        e.setResponseString(textResponse);
        return e;
    }

    public OpenIdConnectRESTHelperException(String message) {
        super(message);
    }

    public OpenIdConnectRESTHelperException(String message, Exception cause) {
        super(message, cause);
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public boolean hasJSONPayload() {
        return getContentType().endsWith("json");
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }

    public String getResponseString() {
        return responseString;
    }

}
