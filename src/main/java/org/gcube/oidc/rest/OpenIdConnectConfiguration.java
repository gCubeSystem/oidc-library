package org.gcube.oidc.rest;

import java.net.URL;

public interface OpenIdConnectConfiguration {

    String getScope();

    String getPortalClientId();

    String getPortalClientSecret();

    URL getIssuerURL();

    URL getLogoutURL();

    URL getTokenURL();

    URL getAuthorizationURL();

    URL getAvatarURL();

}