package org.gcube.oidc;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractOIDCToSitesAndRolesMapper implements OIDCToSitesAndRolesMapper {

    protected static final Logger logger = LoggerFactory.getLogger(OIDCToSitesAndRolesMapper.class);

    protected Map<String, List<String>> resourceName2AccessRoles;

    public AbstractOIDCToSitesAndRolesMapper(Map<String, List<String>> resourceName2AccessRoles) {
        super();
        this.resourceName2AccessRoles = resourceName2AccessRoles;
        logger.debug("Resource name to access roles: {}", resourceName2AccessRoles);
    }

}