package org.gcube.oidc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SlashSeparatedContextMapper extends AbstractOIDCToSitesAndRolesMapper {

    protected static final Logger logger = LoggerFactory.getLogger(SlashSeparatedContextMapper.class);

    private static final String SPLIT_REGEXP = "/";

    private static final int EMPTY_TOKEN_INDEX = 0;

    private static final int ROOT_VO_TOKEN_INDEX = EMPTY_TOKEN_INDEX + 1;

    private static final int VO_TOKEN_INDEX = ROOT_VO_TOKEN_INDEX + 1;

    private static final int VRE_TOKEN_INDEX = VO_TOKEN_INDEX + 1;

    public SlashSeparatedContextMapper(Map<String, List<String>> resourceName2AccessRoles) {
        super(resourceName2AccessRoles);
    }

    @Override
    public Site map(String rootSite) throws SitesMapperExecption {
        List<String> sites = new ArrayList<String>(resourceName2AccessRoles.keySet());
        Collections.sort(sites);
        // Sorting sites, the containers site should come before contained one
        Site gwSitesTree = null;
        for (String site : sites) {
            logger.debug("Checking site: " + site);
            List<String> roles = resourceName2AccessRoles.get(site);
            logger.debug("Roles for site are: {}", roles);
            String[] siteTokens = site.split(SPLIT_REGEXP);
            logger.debug("Tokens are: {}", siteTokens.length);
            if (siteTokens.length < 2) {
                logger.debug("Skipping resource not starting with '{}'", SPLIT_REGEXP);
                continue;
            }
            String rootVO = siteTokens[ROOT_VO_TOKEN_INDEX];
            logger.debug("Root VO is: {}", rootVO);
            if (!rootSite.equals(rootVO)) {
                logger.debug("Skipping evaluation of site tree not belonging to this Root VO: {}", rootVO);
                continue;
            } else {
                logger.debug("Site belongs to this Root VO");
            }
            if (siteTokens.length >= VO_TOKEN_INDEX + 1) {
                if (gwSitesTree == null) {
                    logger.warn(rootVO + " Root VO's permissions are not set for user");
                    gwSitesTree = new Site(rootVO, null);
                }
                String vo = siteTokens[VO_TOKEN_INDEX];
                logger.debug("VO is: {}", vo);
                if (siteTokens.length == VRE_TOKEN_INDEX + 1) {
                    if (!gwSitesTree.getChildren().containsKey(vo)) {
                        logger.warn(vo + " VO's permissions are not set for user");
                        gwSitesTree.getChildren().put(vo, new Site(vo, null));
                    }
                    String vre = siteTokens[VRE_TOKEN_INDEX];
                    logger.debug("VRE is: {}", vre);
                    logger.debug("Adding leaf site: {}", vre);
                    gwSitesTree.getChildren().get(vo).getChildren().put(vre, new Site(vre, roles));
                } else if (!gwSitesTree.getChildren().containsKey(vo)) {
                    logger.debug("Creating site for VO: {}", vo);
                    gwSitesTree.getChildren().put(vo, new Site(vo, roles));
                }
            } else {
                if (gwSitesTree == null) {
                    logger.debug("Creating site for Root VO: {}", rootVO);
                    gwSitesTree = new Site(rootVO, roles);
                } else {
                    if (gwSitesTree.getRoles() == null) {
                        logger.debug("Setting out of order roles for Root VO");
                    } else {
                        logger.warn("Duplicated roles definition for Root VO");
                    }
                }
            }
        }
        return gwSitesTree;
    }
}