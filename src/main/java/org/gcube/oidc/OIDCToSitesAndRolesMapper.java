package org.gcube.oidc;

public interface OIDCToSitesAndRolesMapper {

    Site map(String rootSite) throws SitesMapperExecption;

}