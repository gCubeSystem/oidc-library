package org.gcube.oidc;

public class SitesMapperExecption extends Exception {

    private static final long serialVersionUID = 956917439135370558L;

    public SitesMapperExecption() {
    }

    public SitesMapperExecption(String message) {
        super(message);
    }

    public SitesMapperExecption(Throwable cause) {
        super(cause);
    }

    public SitesMapperExecption(String message, Throwable cause) {
        super(message, cause);
    }

    public SitesMapperExecption(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
