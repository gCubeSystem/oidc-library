This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "oidc-library"

## [v1.3.3]
- Added JSON parsed instance of the refresh token to `JWTToken` class, the getter and methods to checks its presence and expiration

## [v1.3.2] - 2024-05-08
- Header X-D4Science-Context in query exchange and refresh
- Token exchange (#27099)
- PerformQueryTokenWithPOST accepts also optional headers
- PerformURLEncodedPOSTSendData accepts also optional headers
- Moved from `maven-portal-bom` to `gcube-bom`

## [v1.3.1] - 2022-07-07
- Added `Catalogue-Manager` and `Catalogue-Moderator` roles to the enum (#23623)

## [v1.3.0] - 2021-06-24
- Added method to retrieve UMA token by using `clientId` and `clientSecret` in a specific `audience` (aka context) that can now be provided in both encoded and not encoded form (starts with "/" check is performed).

## [v1.2.1] - 2021-04-12
- "Data-Editor" role added (#20896), some logs changed to debug level

## [v1.2.0] - 2021-01-27
- Site is now comparable. Added JTI value getter and essentials info dump in JWT. Revised get avatar helper.

## [v1.1.1]
- New checks for "invalid_grant" and "access_denied" error together with ""Token is not active"", "Invalid bearer token" and "not_authorized" descriptions (#20407)

## [v1.1.0]
- Added avatar configuration and retrieve helper methods (#19726)

## [v1.0.0]
- First release (#19225) (#19226) (#19227)
